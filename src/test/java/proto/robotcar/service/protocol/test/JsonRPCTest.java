package proto.robotcar.service.protocol.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import proto.robotcar.service.protocol.*;

public class JsonRPCTest {

	private Gson serializer;
	private Gson deserializer;

	@Before
	public void setUp() throws Exception {
		 serializer = new GsonBuilder().registerTypeAdapter(JsonRPCParam.class, new JsonRPCParamSerializer()).create();
		 deserializer = new GsonBuilder().registerTypeAdapter(JsonRPCParam.class, new JsonRPCParamDeserializer()).create();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRoundTrip() {
		
		String INPUT = "{\"jsonrpc\":\"2.0\",\"method\":\"start\",\"id\":1,\"params\":[{\"gear\":1},{\"user\":\"John\"}]}";
		
		JsonRPCMessage request = JsonRPCMessage.createMethod("start", 1);		
		request.addParam("gear", 1);
		request.addParam("user", "John");

		String json = serializer.toJson(request);
		
		assertTrue(json.equals(INPUT));
		
		JsonRPCMessage obj = deserializer.fromJson(json, JsonRPCMessage.class);
		
		assertTrue(request.getMethod().equals(obj.getMethod()));
		assertTrue(request.getId() == obj.getId());
		
		assertTrue(request.getParams().size() == 2);
		
	}

}
