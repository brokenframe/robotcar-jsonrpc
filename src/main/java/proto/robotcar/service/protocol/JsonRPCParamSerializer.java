package proto.robotcar.service.protocol;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JsonRPCParamSerializer implements JsonSerializer<JsonRPCParam> {

	@Override
	public JsonElement serialize(JsonRPCParam src, Type typeOfSrc, JsonSerializationContext context) {
		// TODO Auto-generated method stub
		JsonObject object = new JsonObject();
		
		if(src.getValue() instanceof Integer) {
			object.addProperty(src.getName(), (Number)src.getValue());			
		}
		else {
			object.addProperty(src.getName(), src.getValue().toString());			
		}
		
		return object;
	}

}
