package proto.robotcar.service.protocol;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

public class JsonRPCParamDeserializer implements JsonDeserializer<JsonRPCParam> {

	@Override
	public JsonRPCParam deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		// TODO Auto-generated method stub
		
		String paramName = json.getAsJsonObject().keySet().iterator().next();
		Object paramValue = json.getAsJsonObject().get(paramName);

		if(paramValue instanceof JsonPrimitive) {
			JsonPrimitive p = (JsonPrimitive) paramValue;
			if(p.isNumber()) {
				return new JsonRPCParam(paramName, p.getAsInt());							
			}
			else if (p.isString()) {
				return new JsonRPCParam(paramName, p.getAsString());											
			}
			else {
				return new JsonRPCParam(paramName, paramValue);							
			}
		}
				
		else {
			return new JsonRPCParam(paramName, paramValue);			
		}
		
	}

}
