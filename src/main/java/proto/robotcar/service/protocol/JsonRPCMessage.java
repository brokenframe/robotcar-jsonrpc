package proto.robotcar.service.protocol;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonRPCMessage {

	private final static String SPLITTER = "::";
	private String jsonrpc = "2.0";
	private String method;
	private Object result;
	private int id;
	private List<JsonRPCParam> params;
	
	private static Gson deserializer = new GsonBuilder().registerTypeAdapter(JsonRPCParam.class, new JsonRPCParamDeserializer()).create();
	private static Gson serializer = new GsonBuilder().registerTypeAdapter(JsonRPCParam.class, new JsonRPCParamSerializer()).create();

	
	private JsonRPCMessage(String method, int id) {
		this.method = method;
		this.id = id;
	}

	private JsonRPCMessage(Object result, int id) {
		this.result = result;
		this.id = id;
	}

	public static JsonRPCMessage createMethod(String method, int id) {
		return new JsonRPCMessage(method, id);
	}

	public static JsonRPCMessage createResult(Object result, int id) {
		return new JsonRPCMessage(result, id);
	}

	public static JsonRPCMessage fromJson(String json) {
		return deserializer.fromJson(json, JsonRPCMessage.class);
	}
	
	public static String toJson(JsonRPCMessage message) {
		return serializer.toJson(message);
	}
	
	public String getMethod() {
		return method;
	}



	public void setMethod(String method) {
		this.method = method;
	}



	public Object getResult() {
		return result;
	}



	public void setResult(Object result) {
		this.result = result;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}
	
	public String getSimpleMethodName() {
		if(method.contains(SPLITTER)) {
			return method.split(SPLITTER)[1];
		}
		else {
			return method;
		}
	}


	public void addParam(String name, Object value) {
		if(params == null) {
			params = new ArrayList<JsonRPCParam>();
		}
		params.add(new JsonRPCParam(name, value));
	}
	
	public List<JsonRPCParam> getParams() {
		return params;
	}
}
