package proto.robotcar.service.protocol;

public class JsonRPCParam {

	public JsonRPCParam(String name, Object value) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}

	private String name;
	private Object value;
	
	
}
